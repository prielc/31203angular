import { Component, OnInit } from '@angular/core';
import { MessagesService } from '../messages/messages.service';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';

@Component({
  selector: 'app-messagesf',
  templateUrl: './messagesf.component.html',
  styleUrls: ['./messagesf.component.css']
})
export class MessagesfComponent implements OnInit {

  messages;

  constructor(private service:MessagesService) { }

  ngOnInit() { //method that act when the component is created
    this.service.getMessagesFire().subscribe(response=>{
      console.log(response);
      this.messages = response;
    })
  }

}
