import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl} from '@angular/forms';
import { MessagesService } from '../messages/messages.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  invalid = false;
  loginform = new FormGroup({ //בונה מבנה נתונים שתואם לטופס שיצרנו. ניתן להכניס כאן חוקי ולידציה לטופס.
    user: new FormControl(),
    password: new FormControl(),
  });

  login(){
    this.service.login(this.loginform.value).subscribe(response=>{
      this.router.navigate(['/']); //אם ההתחברות הצליחה, המשתמש מועבר לדף הראשי (messages)
    },error =>{ //אם ההתחברות נכשלה
      this.invalid = true;
    })
  }

  logout(){
    localStorage.removeItem('token');
    this.invalid = false;
  }

  constructor(private service:MessagesService, private router:Router) { }

  ngOnInit() {
  }

}