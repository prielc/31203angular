import { MessagesService } from './../messages.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {

  message;

  //מוגדר רק כאשר מבצעים dependacy injection
  //גורם לאנגולר לייצר אובייקט חדש וליצור תכונה של אותו אובייקט
  constructor(private route: ActivatedRoute , private service:MessagesService) { }

  //קוד שפועל כאשר הקומפוננט נוצר
  ngOnInit() {
    this.route.paramMap.subscribe(params=>{
      let id = params.get('id');
      console.log(id);
      this.service.getMessage(id).subscribe(response=>{
        this.message = response.json();
        console.log(this.message);
      })
    })
  }

}