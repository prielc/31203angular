import { MessagesService } from './../messages.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {FormGroup, FormControl} from '@angular/forms';

@Component({
  selector: 'messagesForm',
  templateUrl: './messages-form.component.html',
  styleUrls: ['./messages-form.component.css']
})
export class MessagesFormComponent implements OnInit {

  @Output() addMessage:EventEmitter<any> = new EventEmitter<any>();//we build the tashtit that help us to transform information from the son (message form) to the father (message)
  @Output() addMessagePs:EventEmitter<any> = new EventEmitter<any>();//we build the tashtit that help us to transform information from the son (message form) to the father (message)
  service:MessagesService;

  msgform = new FormGroup({ //בונה מבנה נתונים שתואם לטופס שיצרנו. ניתן להכניס כאן חוקי ולידציה לטופס.
    message: new FormControl(),
    user: new FormControl(),
  });

  sendData(){
    this.addMessage.emit(this.msgform.value.message);//tell to all listenrs the 'add message' hitrachesh, and send the 'this.msgform.value.message'
    
    console.log(this.msgform.value);//לוקח את הערכים שהכנסנו לטופס ושולח אותם לקונסול
    this.service.postMessage(this.msgform.value).subscribe(
      response =>{
        console.log(response.json());
        this.addMessagePs.emit();
      }
    )
  };

  constructor(service:MessagesService) { 
    this.service = service;
  }

  ngOnInit() {
  }

}
