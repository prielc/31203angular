import { Component, OnInit } from '@angular/core';
import { MessagesService } from './messages.service';

@Component({
  selector: 'messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {
  
  //messages = ['a','b','c'];
  messages;
  messagesKeys = [];

  constructor(private service:MessagesService) {
    //let service = new MessagesService();
    service.getMessages().subscribe(response=>{
      //console.log(response.json());
      this.messages = response.json();
      this.messagesKeys = Object.keys(this.messages);
    });
   }

   optimisticAdd(message){
    //console.log("addMessage worked " + message);
    var newKey = parseInt(this.messagesKeys[this.messagesKeys.length-1],0)+1; //take the last id and create a new one
    var newMessageObject = {}; //create message object
    newMessageObject['body'] = message; //put the message on body
    this.messages[newKey] = newMessageObject; //add the new message to the other
    this.messagesKeys = Object.keys(this.messages); //refresh the messagesKey, and add the new message
  }

  pasimisticAdd(){ //תזמון
    this.service.getMessages().subscribe(response=>{
      //console.log(response.json());
      this.messages = response.json();
      this.messagesKeys = Object.keys(this.messages);
    });
  }

  deleteMessage(key){
    console.log.apply(key);
    let index = this.messagesKeys.indexOf(key);
    this.messagesKeys.splice(index,1);
    //delete from server
    this.service.deleteMessage(key).subscribe(
      response=>console.log(response)
    );
  }
  
  ngOnInit() {
  }

}
