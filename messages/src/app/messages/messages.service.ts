import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import {HttpParams} from '@angular/common/http';
import {AngularFireDatabase} from 'angularfire2/database';
import 'rxjs/Rx';



@Injectable()
export class MessagesService {
  http:Http;
  getMessages(){
    //return ['a','b','c'];
    //get messages from the SLIM rest API (Don't say DB)
    let token = localStorage.getItem('token');
    let options = {
      headers: new Headers({
        'Authotization':'Bearer '+token
      })
    }
    return  this.http.get('http://localhost/31023/lesson/slim/messages',options);
  }

  getMessage(id){
    return  this.http.get('http://localhost/31023/lesson/slim/messages/'+id);
  }

  postMessage(data){//input:Json, output: (key,value) //send the data to the server
    //המרת גייסון למפתח וערך
    let options = {
      headers: new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    let params = new HttpParams().append('message',data.message);
    return this.http.post('http://localhost/31023/lesson/slim/messages', params.toString(), options);
  } 
  deleteMessage (key){
    return this.http.delete('http://localhost/31023/lesson/slim/messages/'+key)
  }

  login (credentials){
    let options = {
      headers: new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    let params = new HttpParams().append('user',credentials.user).append('password',credentials.password);
    return this.http.post('http://localhost/31023/lesson/slim/auth',params.toString(),options).map(response=>{
      let token = response.json().token;
      if (token) localStorage.setItem('token',token);
      console.log('in loginform service');
      console.log(token);
    });
  }

   //service that connect to firebase
   getMessagesFire(){
     return this.db.list('/messages').valueChanges();
   }

  constructor(http:Http, private db:AngularFireDatabase) { 
    this.http = http;
  }
}