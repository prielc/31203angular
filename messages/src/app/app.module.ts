import { environment } from './../environments/environment';
import {RouterModule} from '@angular/router';
import { HttpModule } from '@angular/http';
import { MessagesService } from './messages/messages.service';
import { UsersService } from './users/users.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';

import { AppComponent } from './app.component';
import { MessagesComponent } from './messages/messages.component';
import { UsersComponent } from './users/users.component';
import { MessagesFormComponent } from './messages/messages-form/messages-form.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { NavigationComponent } from './navigation/navigation.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { MessageComponent } from './messages/message/message.component';
import { UserComponent } from './users/user/user.component';
import { LoginComponent } from './login/login.component';
import {AngularFireModule} from 'angularfire2';
import {AngularFireDatabaseModule} from 'angularfire2/database';
import { MessagesfComponent } from './messagesf/messagesf.component';


@NgModule({
  declarations: [
    AppComponent,
    MessagesComponent,
    UsersComponent,
    MessagesFormComponent,
    NavigationComponent,
    NotFoundComponent,
    MessageComponent,
    UserComponent,
    LoginComponent,
    MessagesfComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    RouterModule.forRoot([
      {path:'', component:MessagesComponent}, //default - localhost:4200 - homepage
      {path:'users', component:UsersComponent}, //localhost:4200/users
      {path:'message/:id', component:MessageComponent}, //localhost:4200/message/id
      {path:'user/:id', component:UserComponent}, //localhost:4200/user/id
      {path:'login', component:LoginComponent},
      {path:'messagesf', component:MessagesfComponent},
      {path:'**', component:NotFoundComponent} //all the routs that donwt exist
    ])
  ],
  providers: [
    MessagesService,
    UsersService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
