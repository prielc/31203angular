import { Component, OnInit } from '@angular/core';
import { UsersService } from './users.service';

@Component({
  selector: 'users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users;
  usersKeys;

  constructor(private service:UsersService) {
    service.getUsers().subscribe(response=>{
      this.users = response.json();
      this.usersKeys = Object.keys(this.users);
    });
   }

   deleteUser(key){
    console.log.apply(key);
    let index = this.usersKeys.indexOf(key);
    this.usersKeys.splice(index,1);
    //delete from server
    this.service.deleteUser(key).subscribe(
      response=>console.log(response)
    );
  }

  ngOnInit() {
  }

}