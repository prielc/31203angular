import { ActivatedRoute } from '@angular/router';
import { UsersService } from './../users.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  user;

  constructor(private route: ActivatedRoute, private service:UsersService) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params=>{
      let id = params.get('id');
      console.log(id);
      this.service.getUser(id).subscribe(response=>{
        this.user = response.json();
        console.log(this.user);
      })
    })
  }

}
