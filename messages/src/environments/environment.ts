// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyBLBIZJGbM_Be6CVA1JYOQ0BiHocaaRL5k",
    authDomain: "messages-776ba.firebaseapp.com",
    databaseURL: "https://messages-776ba.firebaseio.com",
    projectId: "messages-776ba",
    storageBucket: "messages-776ba.appspot.com",
    messagingSenderId: "402752132707"
  }
};
