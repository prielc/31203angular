<?php
require "bootstrap.php";
use Chatter\Models\Message; 
use Chatter\Models\User;
use Chatter\Middleware\Logging; 

$app = new \Slim\App();
$app->add(new Logging()); //add the midleware (Logging) layer

//lines 8-16 is lesson 2
$app->get('/hello/{name}', function($request, $response,$args){
    return $response->write('Hello '.$args['name']);
});
$app->get('/customer/{number}', function($request, $response,$args){
    return $response->write('customer '.$args['number']. ' welcome to JCE');
});
$app->get('/customer/{cnumber}/product/{pnumber}', function($request, $response,$args){
    return $response->write('customer '.$args['cnumber']. ' product '.$args['pnumber']);
});

//READ MESSAGE - get the message information from database
$app->get('/messages', function($request, $response,$args){
    $_message = new Message();
    $messages = $_message->all();
    $payload = [];
    foreach($messages as $msg){
        $payload[$msg->id] = [
            'body'=>$msg->body,
            'user_id'=>$msg->user_id,
            'created_at'=>$msg->created_at
        ];
    }
    return $response->withStatus(200)->withJson($payload);
});

//READ MESSAGE with ID - get the message information from database
$app->get('/messages/{id}', function($request, $response,$args){
    $_id = $args['id'];
    $message = Message::find($_id);
    return $response->withStatus(200)->withJson($message);
});

//CREATE MESSAGE - insert message to database
$app->post('/messages', function($request, $response,$args){
    $message = $request->getParsedBodyParam('message',''); //the body sheuzan
    $userid = $request->getParsedBodyParam('userid',''); //the body sheuzan
    $_message = new Message(); //create new message
    $_message->body = $message;
    $_message->user_id = $userid;
    $_message->save();
    if($_message->id){
        $payload = ['message_id'=>$_message->id];
        return $response->withStatus(201)->withJson($payload);
    } else {
        return $response->withStatus(400);
    }
});

//DELETE MESSAGE - delete message from database
$app->delete('/messages/{message_id}', function($request, $response,$args){
    $message = Message::find($args['message_id']); //find the message in database
    $message->delete(); //delete the message
    if($message->exists){ //check if the message already exit
        return $response->withStatus(400); //exist -> false error
    } else {
        return $response->withStatus(200); //message delete succesfully
    }
});

//PUT MESSAGE - update message from database
$app->put('/messages/{message_id}', function($request, $response,$args){
    $message = $request->getParsedBodyParam('message',''); //get the parameters from the post
    $_message = Message::find($args['message_id']); // search the message with the id from the url
    $_message->body = $message; //delete the old param, and insert the new param
    if($_message->save()){ //the update succeed
        $payload = ['message_id'=>$_message->id, "result"=>"The message has been update succesfully"]; //useful to the json that return to the user about the update status
        return $response->withStatus(201)->withJson($payload); //succes
    } else{
        return $response->withStatus(400); //error
    }
});

//BULK MESSAGE - insert a number of messages into the DB, with JSON input
$app->post('/messages/bulk', function($request, $response,$args){
    $payload = $request->getParsedBody(); //convert the JSON to array of
    Message::insert($payload); //insert to the DB all what come from the JSON
    return $response->withStatus(201)->withJson($payload);
});

//READ USER - get the user information from database
$app->get('/users', function($request, $response,$args){
    $_user = new User(); //create new message
    $users = $_user->all();
    $payload = [];
    foreach($users as $usr){
        $payload[$usr->id] = [
            'username'=>$usr->username,
            'email'=>$usr->email
        ];
    }
    return $response->withStatus(200)->withJson($payload);
});

//READ USER with ID - get the user information from database
$app->get('/users/{id}', function($request, $response,$args){
    $_id = $args['id'];
    $user = User::find($_id);
    return $response->withStatus(200)->withJson($user);
});

//CREATE USER - insert user to database
$app->post('/users', function($request, $response,$args){
    $user = $request->getParsedBodyParam('user',''); //the user sheuzan
    $mail = $request->getParsedBodyParam('mail',''); //the user sheuzan
    $_user = new user(); //create new user
    $_user->username = $user;
    $_user->email = $mail;
    $_user->save();
    if($_user->id){
        $payload = ['user_id'=>$_user->id];
        return $response->withStatus(201)->withJson($payload);
    } else {
        return $response->withStatus(400);
    }
});

//DELETE USER - delete user from database
$app->delete('/users/{user_id}', function($request, $response,$args){
    $user = User::find($args['user_id']); //find the user in database
    $user->delete(); //delete the user
    if($user->exists){ //check if the user already exit
        return $response->withStatus(400); //exist -> false error
    } else {
        return $response->withStatus(200); //user delete succesfully
    }
});

//PUT USER - update user from database
$app->put('/users/{user_id}', function($request, $response,$args){
    $user = $request->getParsedBodyParam('user',''); //get the parameters from the post
    $_user = User::find($args['user_id']); // search the user with the id from the url
    $_user->username = $user; //delete the old param, and insert the new param
    if($_user->save()){ //the update succeed
        $payload = ['user_id'=>$_user->id, "result"=>"The user has been update succesfully"]; //useful to the json that return to the user about the update status
        return $response->withStatus(201)->withJson($payload); //succes
    } else{
        return $response->withStatus(400); //error
    }
});

//BULK USER - insert a number of users into the DB, with JSON input
$app->post('/users/bulk', function($request, $response,$args){
    $payload = $request->getParsedBody(); //convert the JSON to array of
    User::insert($payload); //insert to the DB all what come from the JSON
    return $response->withStatus(201)->withJson($payload);
});

$app->post('/auth', function($request, $response,$args){
    $user = $request->getParsedBodyParam('user','');
    $password = $request->getParsedBodyParam('password','');
    //we need to go to the DB, but not now
    if($user == 'jack' && $password == '1234'){
        //create JWT and send to client (mow we did it phisicly, we need to do this automaticly)
        //we need to generate JWT,but not now.
        $payload = ['token'=>'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3IiwibmFtZSI6ImphY2siLCJhZG1pbiI6dHJ1ZX0.PETyPBssJTN1V8fSwPxkE0Hftg3hGYux4Rx1WcMC5NA'];
        return $response->withStatus(201)->withJson($payload); //succes
        } else{
            $payload = ['token'=>null];
            return $response->withStatus(403)->withJson($payload); //error
        } 
});

$app->add(new \Slim\Middleware\JwtAuthentication([
    "secret" => "supersecret",
    "path" => ['/users']
]));

$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});

$app->run();